package com.vuitton.util;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
 
@ConfigurationProperties(prefix = "orderShipping")
@Component
public class ShippingProperties {
    private int count_threshold;
    private long job_schedule;
    private Map<String, Integer> orderShippingStoreMap;

    /**
     * @return the orderShippingStoreMap
     */
    public Map<String, Integer> getOrderShippingStoreMap() {
        return orderShippingStoreMap;
    }

    /**
     * @param orderShippingStoreMap the orderShippingStoreMap to set
     */
    public void setOrderShippingStoreMap(Map<String, Integer> orderShippingStoreMap) {
        this.orderShippingStoreMap = orderShippingStoreMap;
    }

    /**
     * @return the job_schedule
     */
    public long getJob_schedule() {
        return job_schedule;
    }

    /**
     * @param job_schedule the job_schedule to set
     */
    public void setJob_schedule(long job_schedule) {
        this.job_schedule = job_schedule;
    }

    /**
     * @return the count_threshold
     */
    public int getCount_threshold() {
        return count_threshold;
    }

    /**
     * @param count_threshold the count_threshold to set
     */
    public void setCount_threshold(int count_threshold) {
        this.count_threshold = count_threshold;
    }

}