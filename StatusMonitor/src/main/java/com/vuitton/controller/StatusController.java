package com.vuitton.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.vuitton.entity.ShippingStatus;
import com.vuitton.service.IShippingStatusService;

@Controller
@RequestMapping("countries")
public class StatusController {
    @Autowired
    private IShippingStatusService shippingStatusService;
    @GetMapping("statuses")
    public ResponseEntity<List<ShippingStatus>> getAllShippingStatus() {
        List<ShippingStatus> list = shippingStatusService.getAllShippingStatus();
        return new ResponseEntity<List<ShippingStatus>>(list, HttpStatus.OK);

    }

    // fetches specific shipping statuses for all stores
    @GetMapping("statuses/{status}")
    public ResponseEntity<List<ShippingStatus>> getAllCountryShippingStatus(@PathVariable("status") String status) {
        List<ShippingStatus> list = shippingStatusService.getAllCountryShippingStatus(status);
        return new ResponseEntity<List<ShippingStatus>>(list, HttpStatus.OK);

    }

    // fetches all statuses for a specific store
    @GetMapping("{country}/statuses")
    public ResponseEntity<List<ShippingStatus>> getAllShippingStatusForCountry(@PathVariable("country") String country) {
        List<ShippingStatus> list = shippingStatusService.getAllShippingStatusForCountry(country);
        return new ResponseEntity<List<ShippingStatus>>(list, HttpStatus.OK);

    }

    // fetches specific status for a specific store
    @GetMapping("{country}/status/{status}")
    public ResponseEntity<List<ShippingStatus>> getCountryShippingStatus(@PathVariable("country") String country,
            @PathVariable("status") String status) {
        List<ShippingStatus> list = shippingStatusService.getCountryShippingStatus(country, status);
        return new ResponseEntity<List<ShippingStatus>>(list, HttpStatus.OK);

    }

} 