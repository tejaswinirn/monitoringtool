package com.vuitton.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vuitton.entity.ShippingStatus;
import com.vuitton.entity.ShippingStatusRowMapper;
@Transactional
@Repository
public class ShippingStatusDAO implements IShippingStatusDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<ShippingStatus> getAllStatusCount() {

        String sql =
                "SELECT country, shipping_group_status , count FROM Country_ShippingType_Count where DATE(modified_timestamp) =(select DATE(MAX(modified_timestamp)) from Country_ShippingType_Count);";
        RowMapper<ShippingStatus> rowMapper = new ShippingStatusRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }

}
