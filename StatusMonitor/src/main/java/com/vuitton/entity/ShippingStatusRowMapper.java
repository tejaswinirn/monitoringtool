package com.vuitton.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ShippingStatusRowMapper implements RowMapper<ShippingStatus> {

    public ShippingStatus mapRow(ResultSet row, int rowNum) throws SQLException {
        ShippingStatus shippingStatus = new ShippingStatus();
        shippingStatus.setCountry(row.getString("country"));
        shippingStatus.setCount(row.getInt("count"));
        shippingStatus.setShippinGroupStatus(row.getString("shipping_group_status"));

        return shippingStatus;
	}

}
