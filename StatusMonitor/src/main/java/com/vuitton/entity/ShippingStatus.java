package com.vuitton.entity;

public class ShippingStatus {
    private String country;
    private String shippinGroupStatus;
    private int count;
    private boolean generateAlert;

    /**
     * @return the generateAlert
     */
    public boolean isGenerateAlert() {
        return generateAlert;
    }

    /**
     * @param generateAlert the generateAlert to set
     */
    public void setGenerateAlert(boolean generateAlert) {
        this.generateAlert = generateAlert;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the shippinGroupStatus
     */
    public String getShippinGroupStatus() {
        return shippinGroupStatus;
    }

    /**
     * @param shippinGroupStatus the shippinGroupStatus to set
     */
    public void setShippinGroupStatus(String shippinGroupStatus) {
        this.shippinGroupStatus = shippinGroupStatus;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }



}