package com.vuitton.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.vuitton.dao.IShippingStatusDAO;
import com.vuitton.entity.ShippingStatus;
import com.vuitton.util.ShippingProperties;

@Service
public class ShippingStatusService implements IShippingStatusService {
    List<ShippingStatus> response;

    @Autowired
    private IShippingStatusDAO shippingStatusDAO;

    @Autowired
    private ShippingProperties shippingProperties;

    @Override
    public List<ShippingStatus> getAllShippingStatusForCountry(String country) {
        List<ShippingStatus> allData = getData();
        List<ShippingStatus> response = new ArrayList<ShippingStatus>();
        if (allData != null) {
            for (ShippingStatus status : allData) {
                if (country.equals(status.getCountry())) {
                    createShippingStatus(status);
                    response.add(createShippingStatus(status));
                }
            }
        }
        return response;
    }


    /**
     * @param status
     * @return ShippingStatus
     */
    private ShippingStatus createShippingStatus(ShippingStatus status) {
        ShippingStatus shippingStatusObj = new ShippingStatus();
        shippingStatusObj.setCount(status.getCount());
        shippingStatusObj.setCountry(status.getCountry());
        shippingStatusObj.setShippinGroupStatus(status.getShippinGroupStatus());
        return shippingStatusObj;
    }

    @Override
    public List<ShippingStatus> getAllCountryShippingStatus(String status) {
        List<ShippingStatus> allData = getData();
        List<ShippingStatus> response = new ArrayList<ShippingStatus>();
        if (allData != null) {
            for (ShippingStatus shippingStatus : allData) {
                if (status.equals(shippingStatus.getShippinGroupStatus())) {
                    createShippingStatus(shippingStatus);
                    response.add(createShippingStatus(shippingStatus));
                }
            }
        }
        return response;
    }

    @Override
    public List<ShippingStatus> getCountryShippingStatus(String country, String status) {
        List<ShippingStatus> allData = getData();
        List<ShippingStatus> response = new ArrayList<ShippingStatus>();
        int threshold = 0;
        if (allData != null) {
            for (ShippingStatus shippingStatus : allData) {
                if (country.equals(shippingStatus.getCountry()) && status.equals(shippingStatus.getShippinGroupStatus())) {
                    if ((shippingProperties.getOrderShippingStoreMap() == null) || shippingProperties.getOrderShippingStoreMap().isEmpty()) {
                        createShippingStatus(shippingStatus);
                        response.add(createShippingStatus(shippingStatus));
                    } else {
                        String keyString = country + "_" + status;
                        ShippingStatus shippingStatusObj = createShippingStatus(shippingStatus);
                        if (shippingProperties.getOrderShippingStoreMap().containsKey(keyString)) {
                            threshold = Integer.valueOf(shippingProperties.getOrderShippingStoreMap().get(keyString));

                            if (shippingStatus.getCount() >= threshold) {
                                shippingStatusObj.setGenerateAlert(true);

                            } else {
                                shippingStatusObj.setGenerateAlert(false);
                            }
                        } else {
                            shippingStatusObj.setGenerateAlert(false);
                        }
                        response.add(shippingStatusObj);
                    }

                }

            }
        }
        return response;
    }

    @Override
    public List<ShippingStatus> getAllShippingStatus() {
        return getData();
    }

    @Scheduled(fixedRateString = "${orderShipping.schedule}")
    /*
     * metod to create a report on periodic basis (frequency configured in application.properties. This report will be returned to the calling
     * method)
     */
    public void createReport() {

        // invoke method to get data from DB and create a response entity object
        List<ShippingStatus> response = getAllData();
        if (response != null) {
            setData(response);
        }

    }

    /**
     * @param response the report to set
     */
    public void setData(List<ShippingStatus> response) {
        this.response = response;
    }

    /**
     * @return the response
     */
    private List<ShippingStatus> getData() {
        return response;
    }

    /**
     * Method to fetch all shipping statuses count for all countries
     */
    @Override
    public List<ShippingStatus> getAllData() {
        return shippingStatusDAO.getAllStatusCount();
    }
}
