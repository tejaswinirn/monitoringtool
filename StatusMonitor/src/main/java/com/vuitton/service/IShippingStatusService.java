package com.vuitton.service;

import java.util.List;

import com.vuitton.entity.ShippingStatus;

public interface IShippingStatusService {

    // fetch report for all shipping statuses for all stores
    List<ShippingStatus> getAllShippingStatus();

    // fetch report for all shipping statuses for a specific store
    List<ShippingStatus> getAllShippingStatusForCountry(String country);

    // fetch report for specific shipping statuses for all stores
    List<ShippingStatus> getAllCountryShippingStatus(String status);

    // fetch report for specific shipping statuses for a specific store
    List<ShippingStatus> getCountryShippingStatus(String country, String status);

    // fetch all data for current date and update the object in memory
    void createReport();

    List<ShippingStatus> getAllData();
}
