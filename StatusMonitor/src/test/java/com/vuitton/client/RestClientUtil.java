package com.vuitton.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.vuitton.entity.ShippingStatus;

public class RestClientUtil {


    public void getAllShippingStatusCountDemo() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/countries/statuses";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<ShippingStatus[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ShippingStatus[].class);
        ShippingStatus[] statuses = responseEntity.getBody();

        for (ShippingStatus status : statuses) {
            System.out.println("Country:" + status.getCountry() + ", Count:" + status.getCount() + ", ShippingStatus: " + status
                    .getShippinGroupStatus());
        }

    }

    public void getAllShippingStatusCountForCountryDemo() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/countries/{country}/statuses";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<ShippingStatus[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ShippingStatus[].class,
                "US");
        ShippingStatus[] statuses = responseEntity.getBody();
        for (ShippingStatus status : statuses) {
            System.out.println("Country:" + status.getCountry() + ", Count:" + status.getCount() + ", ShippingStatus: " + status
                    .getShippinGroupStatus());
        }
    }

    public void getShippingStatusCountForAllCountriesDemo() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/countries/statuses/{status}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<ShippingStatus[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ShippingStatus[].class,
                "test");
        ShippingStatus[] statuses = responseEntity.getBody();
        for (ShippingStatus status : statuses) {
            System.out.println("Country:" + status.getCountry() + ", Count:" + status.getCount() + ", ShippingStatus: " + status
                    .getShippinGroupStatus());
        }
    }

    public void getShippingStatusCountForCountryDemo() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/countries/{country}/status/{status}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<ShippingStatus[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ShippingStatus[].class, "IN",
                "pending_shipment");
        ShippingStatus[] statuses = responseEntity.getBody();
        for (ShippingStatus status : statuses) {
            System.out.println("Country:" + status.getCountry() + ", Count:" + status.getCount() + ", ShippingStatus: " + status
                    .getShippinGroupStatus());
        }
    }
    public static void main(String args[]) {
    	RestClientUtil util = new RestClientUtil();

        util.getAllShippingStatusCountDemo();
        util.getAllShippingStatusCountForCountryDemo();
        util.getShippingStatusCountForAllCountriesDemo();
        util.getShippingStatusCountForCountryDemo();
    }    

   
}
