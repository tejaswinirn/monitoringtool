CREATE DATABASE CountryShippingGroup;

use CountryShippingGroup;

create table Country_ShippingType_Count (
country varchar(255),
shipping_group_status  varchar(255),
count int,
modified_timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
constraint pk_example primary key (country,shipping_group_status,modified_timestamp )
);